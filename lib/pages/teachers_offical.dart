import 'package:flutter/material.dart';

class TeacherOffical extends StatefulWidget {
  const TeacherOffical({Key? key}) : super(key: key);

  @override
  State<TeacherOffical> createState() => _TeacherOfficalState();
}

class _TeacherOfficalState extends State<TeacherOffical> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(
        title: Text('Teachers & offical'),
      ),
      body: Container(

        child: GridView.extent(
          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(
              onPressed: () {},
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text('Director ',
                        style: TextStyle(fontSize: 20)),
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://www.riitpolybd.com/admin-website/images/15833750701574226904Hena%20Sir.jpg',
                        width: 140,
                        height: 100,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '             Abu Hena Md.Mostofa Kamal',
                        style: TextStyle(
                            fontSize: 7,
                            color: Colors.blue.withOpacity(1.0)),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {},
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Head Of the Institute  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://www.riitpolybd.com/admin-website/images/15833748171563185904Principal.jpg',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Md. Shakinur Alam',
                        style: TextStyle(
                            fontSize: 8,
                            color: Colors.blue.withOpacity(1.0)),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Third', style: TextStyle(fontSize: 20)),
              color: Colors.blue,
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Four', style: TextStyle(fontSize: 20)),
              color: Colors.blue,
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Fifth', style: TextStyle(fontSize: 20)),
              color: Colors.blue,
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Six', style: TextStyle(fontSize: 20)),
              color: Colors.blue,
            ),
          ],
        ),
      ),
    ));
  }
}
