import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:riit/pages/home_pages/About_riit.dart';
import 'package:riit/pages/home_pages/academic_riit.dart';
import 'package:riit/pages/home_pages/extra_curriculum_riit.dart';
import 'package:riit/pages/home_pages/job_corner_riit.dart';
import 'package:riit/pages/home_pages/rover_scout_riit.dart';
import 'package:riit/pages/home_pages/study_tour_riit.dart';
import 'package:riit/pages/teachers_offical.dart';


class HomePageTwo extends StatefulWidget {
  const HomePageTwo({Key? key}) : super(key: key);

  @override
  State<HomePageTwo> createState() => _HomePageTwoState();
}

class _HomePageTwoState extends State<HomePageTwo> {
  List ImagesList = [
    {
      "id": 0,
      "image_path": 'assets/images/image1.jpg',
    },
    {"id": 1, "image_path": 'assets/images/image2.jpg'},
    {"id": 2, "image_path": 'assets/images/image3.jpg'},
    {"id": 3, "image_path": 'assets/images/image4.jpg'},
    {"id": 4, "image_path": 'assets/images/image5.jpg'}
  ];

  final CarouselController carouselController = CarouselController();

  int CarentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.deepPurple,
        body: Column(
          children: [
            Stack(
              children: [
                InkWell(
                  onTap: () {},
                  child: CarouselSlider(
                    items: ImagesList.map(
                      (item) => Image.asset(
                        item['image_path'],
                        fit: BoxFit.cover,
                        width: double.infinity,
                      ),
                    ).toList(),
                    carouselController: carouselController,
                    options: CarouselOptions(
                        scrollPhysics: const BouncingScrollPhysics(),
                        autoPlay: true,
                        aspectRatio: 2,
                        viewportFraction: 1,
                        onPageChanged: (index, reason) {
                          setState(() {
                            CarentIndex = index;
                          });
                        }),
                  ),
                ),
                Positioned(
                    bottom: 10,
                    left: 0,
                    right: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: ImagesList.asMap().entries.map((entry) {
                        return GestureDetector(
                          onTap: () =>
                              carouselController.animateToPage(entry.key),
                          child: Container(
                            width: CarentIndex == entry.key ? 17 : 7,
                            height: 7.0,
                            margin: EdgeInsets.symmetric(horizontal: 3.0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: CarentIndex == entry.key
                                    ? Colors.red
                                    : Colors.teal),
                          ),
                        );
                      }).toList(),
                    ))
              ],
            ),
            Container(

              padding: EdgeInsets.only(top: 70, right: 30, left: 30),
              width: MediaQuery.of(context).size.width,
              height: 20,
              color: Colors.deepPurple,
            ),
            Expanded(
              child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        // topLeft: Radius.circular(-500),
                        topRight: Radius.circular(40),
                      )
                      // topLeft: Radius.circular(60))
                      ),
                  child: GridView.extent(
                    primary: false,
                    padding: const EdgeInsets.all(16),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    maxCrossAxisExtent: 200.0,
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>TeacherOffical()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: Text('Teacher & offical ',
                                  style: TextStyle(fontSize: 14)),
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIUSB_aK7ZTtgG_NYW9hL01jZhRpCEhnTj6w&usqp=CAU',
                                  width: 140,
                                  height: 100,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '                              Details',
                                  style: TextStyle(
                                      fontSize: 7,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {},
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Head Of the Institute  ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/15833748171563185904Principal.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Md. Shakinur Alam',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>AcademicRiit()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Academic ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346070eb362924021fdcea748f034e2d9ccc52.png',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> AboutPage()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  About RIIT ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1571545620aboutus2.png',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),

                      MaterialButton(
                        onPressed: () {

                          Navigator.push(context, MaterialPageRoute(builder: (context)=>StudaytourRiit()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Study Tour',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346152study_tour.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),

                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>JobCornerRiit()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Job Corner ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346362job_korner2.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),

                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>RoverScoutRiit()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Rover Scout ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346531boy-bangladesh-scouts-scouting-world-organization-of-the-scout-movement-dhaka-scout-association-world-scout-emblem-rover-scout-png-clipart.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>ExtraCurriculumRiit()));
                        },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Extra curriculum ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346922Make-Extra-Curricular-Activities-Work-For-You-At-MBA-Admissions.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                         },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Innovation Activities ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584348596Innovation_thumb900.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                         },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Emergency Call ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584347206888ab4fb111989961328ff5d93f8216d-emergency-call-vector.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                         },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Service Box',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584347337service-box-logo.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                         },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Blood Bank ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346578blood.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                         },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text(' Project Management ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346300378-Project-management-Logo-Template.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                         },
                        padding: const EdgeInsets.all(8),
                        color: Colors.grey.shade100,
                        child: Column(
                          children: [
                            Text('  Aministration ',
                                style: TextStyle(fontSize: 14)),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1584346442administration-icon-trendy-logo-concept-white-background-business-collection-suitable-use-web-apps-mobile-print-131187535.jpg',
                                  width: 140,
                                  height: 100,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  ' Text',
                                  style: TextStyle(
                                      fontSize: 8,
                                      color: Colors.blue.withOpacity(1.0)),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '            Rangpur Ideal Institute of Technology',
                                  style: TextStyle(fontSize: 6),
                                )
                              ],
                            )
                          ],
                        ),
                      ),



                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
