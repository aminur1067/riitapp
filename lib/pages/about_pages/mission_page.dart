import 'package:flutter/material.dart';
class MissionPage extends StatefulWidget {
  const MissionPage({Key? key}) : super(key: key);

  @override
  State<MissionPage> createState() => _MissionPageState();
}

class _MissionPageState extends State<MissionPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(

      appBar: AppBar(
        title: Text(
          'Mission'
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Text(

            '\n  1:  To develop the unity among teacher -staff-  student to safeguard the interest of the institute'
    '\n  2:  To increase the passing out rate of quality product'
    '\n  3:  To Develop the efficiency of teachers through training'
    '\n  4:  To ensure the infrastructure and logistic facilities'
    '\n  5:  To keep connection with surrounding civil society and authority'
    '\n  6:  To modernize the teaching-learning process and information interchange system'
    '\n  7:  To maintain the industry linkage and ensure the placement for outcome'
    '\n  8:  To develop the mentality and morality through co-curriculum activities'
    '\n  9:  To increase the different facilities for female students'
          ),
      ),
    ));
  }
}
