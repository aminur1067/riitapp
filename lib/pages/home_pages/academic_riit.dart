import 'package:flutter/material.dart';
import 'package:riit/pages/academic_page/Syllabus_page.dart';
import 'package:riit/pages/academic_page/routine_page.dart';

import '../../constants.dart';
import '../academic_page/admission_page.dart';
import '../academic_page/admission_result_page.dart';

class AcademicRiit extends StatefulWidget {
  const AcademicRiit({Key? key}) : super(key: key);

  @override
  State<AcademicRiit> createState() => _AcademicRiitState();
}

class _AcademicRiitState extends State<AcademicRiit> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('amarCempass'),
          actions: [],
        ),
        body: GridView.extent(
          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>AdmissionPage()));
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Admission', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network(
                        'https://png.pngtree.com/png-clipart/20210311/original/pngtree-admissions-open-3d-abstract-logo-png-image_6035477.jpg',
                        height: 95,
                        width: 130,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: [
                      Text(
                        'details',
                        style: TextStyle(fontSize: 10, color: Colors.blue),
                      )
                    ],
                  )
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>AdmissionResultPage()));
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Admission Result', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSgvgowhOwZoR5FN8XUp52aOXgwlKAMsLKrJQ&usqp=CAU',
                        height: 95,
                        width: 130,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: [
                      Text(
                        'details',
                        style: TextStyle(fontSize: 10, color: Colors.blue),
                      )
                    ],
                  )
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>SyllabusPage()));
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Syllabus', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRepGFKkyjSBjH7qyUG2X5BpsOcg36zL3tyGg&usqp=CAU',
                        height: 95,
                        width: 130,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: [
                      Text(
                        'details',
                        style: TextStyle(fontSize: 10, color: Colors.blue),
                      )
                    ],
                  )
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>RoutinePage()));
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Routine', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuC_aMwl7IAaSKxG8e2DvbgOwcJAy9dTYvKA&usqp=CAU',
                        height: 95,
                        width: 130,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  details
                ],
              ),
              color: Colors.grey.shade100,
            ),
          ],
        ),
      ),
    );
  }
}
