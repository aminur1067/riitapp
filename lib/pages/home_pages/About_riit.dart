import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../about_pages/Facilities_page.dart';
import '../about_pages/mission_page.dart';
import '../about_pages/vission_page.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('amarCempass'),
          actions: [],
        ),
        body: GridView.extent(

          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FacilitiesPage()));
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  Text('Facilities', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network(
                        'https://thumbs.dreamstime.com/b/facility-management-logo-concept-facility-management-concept-house-tools-colorful-flat-design-155504085.jpg',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  details
                ],
              ),
              color: Colors.grey.shade300,
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>MissionPage()));
              },
              padding: EdgeInsets.all(8),
              child: Column(
                  children: [
                    Text('Mission', style: TextStyle(fontSize: 20)),
                    Row(
                      children: [
                        Image.network('https://getfullyfunded.com/wp-content/uploads/2019/07/Mission-with-arrow-1024x576.jpg',height: 100,width: 140,)
                      ],
                    ),
                    details

                  ],


              ),

              color: Colors.grey.shade300,
            ),
            MaterialButton(onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>VissionPage()));
            },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Vission', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvrKAtDjlc4sZuRLCQo0b_U2aQz_BrzKlcew&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
                
              ),
              color: Colors.grey.shade300,
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: const Text('Four', style: TextStyle(fontSize: 20)),
              color: Colors.grey.shade300,
            ),
          ],
        ),
      ),
    );
  }
}
