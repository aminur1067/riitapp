import 'package:flutter/material.dart';

import '../../constants.dart';

class StudaytourRiit extends StatefulWidget {
  const StudaytourRiit({Key? key}) : super(key: key);

  @override
  State<StudaytourRiit> createState() => _StudaytourRiitState();
}

class _StudaytourRiitState extends State<StudaytourRiit> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('amarCempass'),
          actions: [

          ],
        ),
        body:  GridView.extent(
          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Study tour\n   2021', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEF5_KcxOSjrs8T9JPne2uYD24b_BMHw6LZA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),

              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Study tour\n   2022', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEF5_KcxOSjrs8T9JPne2uYD24b_BMHw6LZA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),

              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Study tour\n   2023', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEF5_KcxOSjrs8T9JPne2uYD24b_BMHw6LZA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),

              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Study tour\n   2024', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEF5_KcxOSjrs8T9JPne2uYD24b_BMHw6LZA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),

              color: Colors.grey.shade100,
            ),

          ],
        ),
      ),
    );
  }
}
