import 'dart:async';
import 'package:flutter/material.dart';

import 'package:riit/pages/home_page_two.dart';




class SplashScreen extends StatefulWidget {
  const SplashScreen({ Key? key }) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds:5), (){
      Navigator.push(context , MaterialPageRoute(builder: (context) => HomePageTwo()));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
           Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGe5K09O33pXLGXIPKcO7jW00aqBr4LN6JuBFuLeqVBg&s'),
            Text('RIIT',style: TextStyle(color: Colors.deepPurple)),
          ],),

      ),

    );
  }
}
